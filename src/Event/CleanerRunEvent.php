<?php

namespace Drupal\cleaner\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class CleanerRunEvent.
 *
 * @package Drupal\cleaner\Event
 */
class CleanerRunEvent extends Event {

  /**
   * Event name.
   */
  const CLEANER_RUN = 'cleaner.run';

}
